<?php
/**
 * @file
 * Set module specific constants.
 */

/**
 * COMMERCE_BIGFISH_PAYMENTGATEWAY_LIBRARY_NAME.
 */
define('COMMERCE_BIGFISH_PAYMENTGATEWAY_LIBRARY_NAME', 'payment-gateway-php-sdk-master');

/**
 * COMMERCE_BIGFISH_PAYMENTGATEWAY_LIBRARY_URL_DOWNLOAD.
 */
define('COMMERCE_BIGFISH_PAYMENTGATEWAY_LIBRARY_URL_DOWNLOAD', 'https://github.com/bigfish-hu/payment-gateway-php-sdk');

/**
 * COMMERCE_BIGFISH_PAYMENTGATEWAY_LIBRARY_URL_INFO.
 */
define('COMMERCE_BIGFISH_PAYMENTGATEWAY_LIBRARY_URL_INFO', 'https://www.paymentgateway.hu/technikai_informaciok.html');

/**
 * COMMERCE_BIGFISH_PAYMENTGATEWAY_RESPONSE_PATH.
 */
define('COMMERCE_BIGFISH_PAYMENTGATEWAY_RESPONSE_PATH', 'commerce-bf-pmgw-response');
