<?php
/**
 * @file
 * Implements available providers on BIG FISH Paymentgateway.
 */

use \BigFish\PaymentGateway;

/**
 * Available providers on BIG FISH Paymentgateway.
 */
function commerce_bigfish_paymentgateway_providers() {
  _commerce_bigfish_paymentgateway_load_library();

  return array(
    'commerce_bigfish_paymentgateway_barion' => array(
      'title' => t('BIG FISH Payment Gateway to Barion payment service'),
      'description' => t('BIG FISH Payment Gateway to Barion payment service (Hungary)'),
      'provider_id' => PaymentGateway::PROVIDER_BARION2,
    ),
    'commerce_bigfish_paymentgateway_borgun' => array(
      'title' => t('BIG FISH Payment Gateway to Borgun payment service'),
      'description' => t('BIG FISH Payment Gateway to Borgun payment service (Hungary)'),
      'provider_id' => PaymentGateway::PROVIDER_BORGUN,
    ),
    'commerce_bigfish_paymentgateway_cib' => array(
      'title' => t('BIG FISH Payment Gateway to CIB bank'),
      'description' => t('BIG FISH Payment Gateway to CIB bank (Hungary)'),
      'provider_id' => PaymentGateway::PROVIDER_CIB,
    ),
    'commerce_bigfish_paymentgateway_escalion' => array(
      'title' => t('BIG FISH Payment Gateway to Escalion'),
      'description' => t('BIG FISH Payment Gateway to Escalion'),
      'provider_id' => PaymentGateway::PROVIDER_ESCALION,
    ),
    'commerce_bigfish_paymentgateway_fhb' => array(
      'title' => t('BIG FISH Payment Gateway to FHB bank'),
      'description' => t('BIG FISH Payment Gateway to FHB bank (Hungary)'),
      'provider_id' => PaymentGateway::PROVIDER_FHB,
    ),
    'commerce_bigfish_paymentgateway_khb' => array(
      'title' => t('BIG FISH Payment Gateway to KHB bank'),
      'description' => t('BIG FISH Payment Gateway to KHB bank (Hungary)'),
      'provider_id' => PaymentGateway::PROVIDER_KHB,
    ),
    'commerce_bigfish_paymentgateway_khbszep' => array(
      'title' => t('BIG FISH Payment Gateway to KHB bank using "SZÉP" card'),
      'description' => t('BIG FISH Payment Gateway to KHB bank (Hungary) using "SZÉP" card'),
      'provider_id' => PaymentGateway::PROVIDER_KHB_SZEP,
    ),
    'commerce_bigfish_paymentgateway_mkbszep' => array(
      'title' => t('BIG FISH Payment Gateway to MKB bank using "SZÉP" card'),
      'description' => t('BIG FISH Payment Gateway to MKB bank (Hungary) using "SZÉP" card'),
      'provider_id' => PaymentGateway::PROVIDER_MKB_SZEP,
    ),
    'commerce_bigfish_paymentgateway_otp' => array(
      'title' => t('BIG FISH Payment Gateway to OTP bank'),
      'description' => t('BIG FISH Payment Gateway to OTP bank (Hungary)'),
      'provider_id' => PaymentGateway::PROVIDER_OTP,
    ),
    'commerce_bigfish_paymentgateway_otpsimple' => array(
      'title' => t('BIG FISH Payment Gateway to OTP bank - Simple'),
      'description' => t('BIG FISH Payment Gateway to OTP bank - Simple (Hungary)'),
      'provider_id' => PaymentGateway::PROVIDER_OTP_SIMPLE,
    ),
    'commerce_bigfish_paymentgateway_otpsimplewire' => array(
      'title' => t('BIG FISH Payment Gateway to OTP bank - Simple wire'),
      'description' => t('BIG FISH Payment Gateway to OTP bank - Simple wire (Hungary)'),
      'provider_id' => PaymentGateway::PROVIDER_OTP_SIMPLE_WIRE,
    ),
    'commerce_bigfish_paymentgateway_otpay' => array(
      'title' => t('BIG FISH Payment Gateway to OTPay payment service'),
      'description' => t('BIG FISH Payment Gateway to OTPay payment service (Hungary)'),
      'provider_id' => PaymentGateway::PROVIDER_OTPAY,
    ),
    'commerce_bigfish_paymentgateway_otpaymp' => array(
      'title' => t('BIG FISH Payment Gateway to OTPay MasterPass payment service'),
      'description' => t('BIG FISH Payment Gateway to OTPay MasterPass payment service (Hungary)'),
      'provider_id' => PaymentGateway::PROVIDER_OTPAY_MASTERPASS,
    ),
    'commerce_bigfish_paymentgateway_otpmultipont' => array(
      'title' => t('BIG FISH Payment Gateway to OTP Multipont payment service'),
      'description' => t('BIG FISH Payment Gateway to OTP Multipont payment service (Hungary)'),
      'provider_id' => PaymentGateway::PROVIDER_OTP_MULTIPONT,
    ),
    'commerce_bigfish_paymentgateway_paypal' => array(
      'title' => t('BIG FISH Payment Gateway to PayPal'),
      'description' => t('BIG FISH Payment Gateway to PayPal'),
      'provider_id' => PaymentGateway::PROVIDER_PAYPAL,
    ),
    'commerce_bigfish_paymentgateway_paysafecard' => array(
      'title' => t('BIG FISH Payment Gateway to Paysafecard'),
      'description' => t('BIG FISH Payment Gateway to Paysafecard'),
      'provider_id' => PaymentGateway::PROVIDER_PAYSAFECARD,
    ),
    'commerce_bigfish_paymentgateway_payu2' => array(
      'title' => t('BIG FISH Payment Gateway to PayU'),
      'description' => t('BIG FISH Payment Gateway to PayU'),
      'provider_id' => PaymentGateway::PROVIDER_PAYU2,
    ),
    'commerce_bigfish_paymentgateway_qpay' => array(
      'title' => t('BIG FISH Payment Gateway to Wirecard QPAY'),
      'description' => t('BIG FISH Payment Gateway to Wirecard QPAY'),
      'provider_id' => PaymentGateway::PROVIDER_WIRECARD_QPAY,
    ),
    'commerce_bigfish_paymentgateway_saferpay' => array(
      'title' => t('BIG FISH Payment Gateway to Saferpay'),
      'description' => t('BIG FISH Payment Gateway to Saferpay'),
      'provider_id' => PaymentGateway::PROVIDER_SAFERPAY,
    ),
    'commerce_bigfish_paymentgateway_sms' => array(
      'title' => t('BIG FISH Payment Gateway to SMS'),
      'description' => t('BIG FISH Payment Gateway to SMS'),
      'provider_id' => PaymentGateway::PROVIDER_SMS,
    ),
    'commerce_bigfish_paymentgateway_sofort' => array(
      'title' => t('BIG FISH Payment Gateway to Sofort payment service'),
      'description' => t('BIG FISH Payment Gateway to Sofort payment service (Hungary)'),
      'provider_id' => PaymentGateway::PROVIDER_SOFORT,
    ),
    'commerce_bigfish_paymentgateway_unicredit' => array(
      'title' => t('BIG FISH Payment Gateway to Unicredit bank'),
      'description' => t('BIG FISH Payment Gateway to Unicredit bank (Hungary)'),
      'provider_id' => PaymentGateway::PROVIDER_UNICREDIT,
    ),
  );
}
